import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://demo-app.online/daftar')

WebUI.click(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'))

WebUI.setText(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Nama_name'), nama)

WebUI.setText(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Tanggal lahir_birth_date'), 
    tanggal_lahir)

WebUI.click(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/label_E-Mail'))

WebUI.setText(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_E-Mail_email'), email)

WebUI.setText(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Whatsapp_whatsapp'), whatsapp)

WebUI.setText(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Kata Sandi_password'), kata_sandi)

WebUI.setText(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_password_confirmation'), 
    konfirmasi_kata_sandi)

WebUI.click(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/input_Konfirmasi kata sandi_inlineCheckbox1'))

WebUI.click(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/button_Daftar'))

WebUI.verifyElementPresent(findTestObject('Register web/Page_Buat akun dan dapatkan akses di Coding.ID/verify/span_Buat Akun Baru'), 
    0)

