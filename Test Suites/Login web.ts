<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login web</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>7badc5bc-f889-404e-ad0f-d5208fc9b878</testSuiteGuid>
   <testCaseLink>
      <guid>d090f296-8bbf-490c-9aef-2a7d6269b124</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/login - DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>7054e553-a141-4d3f-8aa0-96fcd35437c2</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/New Test Data</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>7054e553-a141-4d3f-8aa0-96fcd35437c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>886a69e2-3721-4285-9aa6-fb8a9c32a271</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>7054e553-a141-4d3f-8aa0-96fcd35437c2</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>297a4915-38cc-42f7-964a-3bd37bc92652</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
